import { NgModule } from '@angular/core';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatButtonModule } from '@angular/material/button';


@NgModule({
    imports: [MatToolbarModule, MatDatepickerModule, MatButtonModule],
    exports: [MatToolbarModule, MatDatepickerModule, MatButtonModule],
})

export class MaterialModule { }
import { Injectable } from '@angular/core';

import { DataCollectorService } from './data-collector.service';

import Map from 'ol/Map';
import View from 'ol/View';
import { Tile as TileLayer, Vector as VectorLayer } from 'ol/layer';
import XYZ from 'ol/source/XYZ';

import { register } from 'ol/proj/proj4';
import proj4 from 'proj4';

import { get as getProjection, fromLonLat, transform } from 'ol/proj';
import Feature from 'ol/Feature';
import Point from 'ol/geom/Point';
import { Fill, Stroke, Circle, Style } from 'ol/style';
import LineString from 'ol/geom/LineString';
import VectorSource from 'ol/source/Vector';


@Injectable({
  providedIn: 'root'
})
export class MapService {

  mapId: string = 'bird-map'
  map: any

  center: []
  markers: any = []
  updatedIds: any = []
  outdatedIds = {}

  features: any = []

  vectorSource = new VectorSource({
    wrapX: false
  })
  vectorLayer = new VectorLayer({
    source: this.vectorSource
  })

  tileLayer = new TileLayer({
    source: new XYZ({
      url: 'https://{a-c}.tile.openstreetmap.org/{z}/{x}/{y}.png'
    })
  })

  proj3044: any

  //vars for simulation and files
  fileMin: number = 7003
  fileMax: number = 8453
  currentFile: number = 7003
  simulation: any

  constructor(
    private Data: DataCollectorService
  ) { }

  generate(data: any) {
    this.generateProjection();
    this.center = this.transformCoordinates(data.coordinates);
    this.markers = data.list;

    this.map = new Map({
      target: this.mapId,
      layers: [
        this.tileLayer
      ],
      view: new View({
        projection: this.proj3044,
        center: this.center,
        zoom: 10,
        extent: this.setMapBoundariesExtent(this.center, 10),
      })
    });

    this.map.addLayer(this.vectorLayer);
    this.setMarkers();
    return this.map;
  }

  transformCoordinates(coordinates: [number, number]) {
    return transform(coordinates, 'EPSG:4326', 'EPSG:3044')
  }

  toggleSimulation(interval: number = 200) {
    if (this.simulation) {
      clearInterval(this.simulation);
      this.simulation = undefined;
    } else {
      this.simulation = setInterval(() => {
        this.Data.getRotationData(this.currentFile)
          .subscribe((res: any) => {
            this.setMarkers(res.list);
          })
        this.currentFile++;
        this.currentFile = this.currentFile > this.fileMax ? this.fileMin : this.currentFile; //prevent getting unexisting files
      }, interval);
    }
    return this.simulation;
  }

  setMapBoundariesExtent(coords: Object, km: number) {
    let unit = km * 1000;
    return [
      coords[0] - unit,
      coords[1] - unit,
      coords[0] + unit,
      coords[1] + unit
    ]
  }

  defaultLineStyle() {
    return new Style({
      stroke: new Stroke({
        color: '#004d99',
        width: 1
      })
    })
  }

  defaultMarkerStyle() {
    return new Style({
      image: new Circle({
        radius: 3,
        fill: new Fill({ color: '#5ed945' }),
        stroke: new Stroke({
          color: '#2e6e21',
          width: 2
        })
      })
    });
  }

  setOutdatedIds(ids: any = [], times: Number) {
    const features = this.vectorSource.getFeatures();

    const outdatedList = features.filter((feature: any) => {
      return ids.indexOf(feature.id_) == -1 && feature.id_.indexOf('line') !== 0;
    }).map((f: any) => f.id_);

    outdatedList.forEach((id: any) => {
      this.outdatedIds[id] = this.outdatedIds[id] == undefined ? 1 : this.outdatedIds[id] + 1; //set the time for an id

      if (this.outdatedIds[id] == times) { //if same id meets declared times value, it will be removed
        this.outdatedIds[id] = null; //remove id from outdated list
        let featureToRemove = this.vectorSource.getFeatureById(id);
        this.removeLines(id);
        this.vectorSource.removeFeature(featureToRemove);
      }
    });
  }

  removeLines(featureId: string) {
    let features = this.vectorSource.getFeatures();
    let lineIds = features.filter((feature: any) => {
      return feature.id_.indexOf(`line-${featureId}`) == 0;
    })
      .map((f: any) => f.id_); //find an id with line prefix to remove

    lineIds.forEach((l: any) => {
      this.vectorSource.removeFeature(this.vectorSource.getFeatureById(l))
    })
  }

  setLines(coordinates: any, id: string) {
    let l = new Feature({
      geometry: new LineString(
        coordinates
      )
    })
    l.setStyle(this.defaultLineStyle);
    l.setId(`line-${id}-${Math.floor(Math.random() * 100000)}`); //set line with feature id, prefix and random number to prevent duplication
    this.vectorSource.addFeature(l);
  }

  setMarkers(markers?: any) {
    if (markers) this.markers = markers;

    this.updatedIds = this.markers.map((m: any) => m.properties.TrackId);
    this.setOutdatedIds(this.updatedIds, 3);

    this.markers.forEach((marker: any) => {
      const id: string = marker.properties.TrackId;

      let existingFeature = this.vectorSource.getFeatureById(id);

      let c = this.transformCoordinates(marker.geometry.coordinates);
      let f = new Feature({
        geometry: new Point(c),
        properties: marker.properties
      });

      f.setId(id);
      f.setStyle(this.defaultMarkerStyle());

      if (!existingFeature) {
        this.vectorSource.addFeature(f);
      } else {
        //if exists
        let geo = existingFeature.getGeometry();
        let pc = geo.flatCoordinates; //previous coordinates
        this.setLines(
          [
            [pc[0], pc[1]], //previous
            [c[0], c[1]] //current
          ],
          id)
        existingFeature.setGeometry(new Point(c))
      }
    });
  }

  move(next: boolean) {
    if (next) {
      this.currentFile++;
    } else {
      this.currentFile -= 1;
      this.vectorSource.clear();
    }
    this.currentFile = this.currentFile > this.fileMax ?
      this.fileMin : this.currentFile < this.fileMin ?
        this.fileMin : this.currentFile; //prevent getting unexisting files

    this.Data.getRotationData(this.currentFile)
      .subscribe((res: any) => {
        this.setMarkers(res.list);
      })
  }

  reset() {
    this.vectorSource.clear();
    this.currentFile = this.fileMin;
    this.Data.getRotationData(this.currentFile)
      .subscribe((res: any) => {
        this.setMarkers(res.list);
      })
  }

  generateProjection() {
    proj4.defs("EPSG:3044", "+proj=utm +zone=32 +ellps=GRS80 +units=m +no_defs");
    register(proj4);
    this.proj3044 = getProjection('EPSG:3044');
    this.proj3044.setExtent([231991.5007, 4065788.6515, 768008.4993, 7768690.1087]);
  }
}

import { Component, OnInit } from '@angular/core';

import { MapService } from '../map.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

  simulating: boolean = false
  simulation: any

  constructor(
    private Map: MapService
  ) { }

  ngOnInit(): void {
  }

  toggleSimulation() {
    this.simulating = !this.simulating;
    this.simulation = this.Map.toggleSimulation(100);
    return this.simulation;
  }

  next() {
    if (this.simulating) this.toggleSimulation();
    this.Map.move(true);
  }

  prev() {
    if (this.simulating) this.toggleSimulation();
    this.Map.move(false);
  }
  reset() {
    if (this.simulating) this.toggleSimulation();
    this.Map.reset();
  }

  itteration() {
    return this.Map.currentFile;
  }
}

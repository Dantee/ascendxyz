import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { MapService } from './map.service';

describe('MapService', () => {
  let service: MapService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });
    service = TestBed.inject(MapService);
    service.generateProjection();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should calculate map extent', () => {
    let kilometers = 50;
    let calculation = service.setMapBoundariesExtent([100000, 100000], kilometers);
    expect(calculation).toEqual(
      [
        100000 - kilometers * 1000,
        100000 - kilometers * 1000,
        100000 + kilometers * 1000,
        100000 + kilometers * 1000
      ]
    );
  });

  it('should get the next file', () => {
    let current = service.currentFile;
    service.move(true);
    expect(service.currentFile).toEqual(current + 1 > service.fileMax ? service.fileMax : current + 1);
  });


  it('should get the previous file', () => {
    let current = service.currentFile;
    service.move(false);
    expect(service.currentFile).toEqual(current - 1 < service.fileMin ? service.fileMin : current - 1);
  });

  it('should add features', () => {
    service.setMarkers([
      {
        properties: {
          TrackId: 'one'
        },
        geometry: {
          coordinates: [1, 1]
        }
      }
    ]);
    let featureCount = service.vectorSource.getFeatures().length;
    expect(featureCount).toEqual(1);
  });


  it('should add a track and a line between first track', () => {
    service.setMarkers([
      {
        properties: {
          TrackId: 'one'
        },
        geometry: {
          coordinates: [1, 1]
        }
      }
    ]);
    service.setMarkers([
      {
        properties: {
          TrackId: 'one'
        },
        geometry: {
          coordinates: [2, 3]
        }
      },
      {
        properties: {
          TrackId: 'two'
        },
        geometry: {
          coordinates: [3, 4]
        }
      }
    ]);
    let features = service.vectorSource.getFeatures()
    let featureCount = features.length;
    let ids = features.map((f: any) => f.id_);
    expect(featureCount).toEqual(3);
    expect(ids.filter((id: string) => id.indexOf('line-') !== -1).length).toEqual(1);
    expect(ids.filter((id: string) => id.indexOf('line-') == -1).length).toEqual(2);

  });
});

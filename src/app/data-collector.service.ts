import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataCollectorService {
  rotations = {
    first: 7003,
    last: 8453
  }
  dataUrl: string = '../assets/track/'
  lastNum: Number
  trackingData: []

  constructor(
    private http: HttpClient
  ) { }

  getRotationData(num?: Number) {
    return new Observable((observer) => {
      num = !num ? this.rotations.first : num;
      num = num < this.rotations.first ? this.rotations.first : num > this.rotations.last ? this.rotations.last : num;
      this.lastNum = num;

      let fileUrl = `${this.dataUrl}${num}.geojson`;
      return this.http.get(fileUrl)
        .subscribe((res: any) => {
          this.trackingData = res.features.slice();
          this.trackingData.shift();
          let coordinates = res.features[0].geometry.coordinates;

          observer.next({
            list: this.trackingData,
            coordinates
          })
        })
    })
  }
}

import { Component, OnInit } from '@angular/core';

import { DataCollectorService } from '../data-collector.service';
import { MapService } from '../map.service';

@Component({
  selector: 'app-bird-map',
  templateUrl: './bird-map.component.html',
  styleUrls: ['./bird-map.component.scss']
})
export class BirdMapComponent implements OnInit {
  map: any
  trackingData: []
  constructor(
    private Data: DataCollectorService,
    private Map: MapService
  ) { }

  ngOnInit(): void {
    this.Data.getRotationData()
      .subscribe((res: any) => {
        this.Map.generate(res);
      });
  }

}

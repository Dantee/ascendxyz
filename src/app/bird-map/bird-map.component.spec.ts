import { HttpClientTestingModule } from '@angular/common/http/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BirdMapComponent } from './bird-map.component';

describe('BirdMapComponent', () => {
  let component: BirdMapComponent;
  let fixture: ComponentFixture<BirdMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [BirdMapComponent],
      imports: [HttpClientTestingModule],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BirdMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
